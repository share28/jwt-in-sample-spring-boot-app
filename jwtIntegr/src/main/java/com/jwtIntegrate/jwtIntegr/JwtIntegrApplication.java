package com.jwtIntegrate.jwtIntegr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserDao.class)
public class JwtIntegrApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtIntegrApplication.class, args);
	}

}
