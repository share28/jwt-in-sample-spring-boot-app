package com.jwtIntegrate.jwtIntegr;

import org.springframework.stereotype.Service;
//import com.mongo.example.mongodemo.models.apimodel.User;
import org.springframework.security.core.userdetails.User;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.ArrayList;


@Service
public class MyUserDetailsService implements UserDetailsService {
    private String SECRET_KEY ="secret";
	
    @Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//    	com.mongo.example.mongodemo.models.apimodel.User user = new com.mongo.example.mongodemo.models.apimodel.User();
//    	 return new User(user.getEmail(), user.getPassword(),
//                 new ArrayList<>());
//    Optional<com.jwtIntegrate.jwtIntegr.User> user = userDao.findByUserName(s);
//    return user.map(CarUser::new).get();
//     new CarUser(s);   
//    	$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6 <=encode <=password
    	
    	if ("abc".equals(s)) {
			return new User("abc", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + s);
		}
   
//    	com.jwtIntegrate.jwtIntegr.User user = (com.jwtIntegrate.jwtIntegr.User) userDao.findByUsername(s);
//		if (user == null) {
//			throw new UsernameNotFoundException("User not found with username: " + s);
//		}
//		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
//				new ArrayList<>());
    }
    public UserDao save(UserDto user) {
		com.jwtIntegrate.jwtIntegr.User newUser = new com.jwtIntegrate.jwtIntegr.User();
		newUser.setUsername(user.getUser());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		com.jwtIntegrate.jwtIntegr.User v = userDao.save(newUser);
		return (UserDao) userDao.save(v);
				
	}
}