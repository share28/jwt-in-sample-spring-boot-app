package com.jwtIntegrate.jwtIntegr;

public class UserDto {

	private String user;
	private String email;   
	private String address;   
	private String img;
	private String password;
	
	public UserDto() {
		super();
	}

	public UserDto(String user, String email, String address, String img,String password) {
		super();
		this.user = user;
		this.email = email;
		this.address = address;
		this.img = img;
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDto [user=" + user + ", email=" + email + ", address=" + address + ", img=" + img + ", password="
				+ password + "]";
	}

	
}