package com.jwtIntegrate.jwtIntegr;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Integer>{
	
	User findUserByEmail(String email);
//window/preferences/spring/project validators/Data 
	UserDao findByUsername(String username);
	Boolean existsByUsername(String username);
	
	@Query(value="SELECT u.user_id,u.address,u.email,u.first_name,u.img,u.last_name FROM user u WHERE u.user_id =:user_id",nativeQuery = true)
	User getUserProfileById(@Param("user_id") int user_id);

	

}
